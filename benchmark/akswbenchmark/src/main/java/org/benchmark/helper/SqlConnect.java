package org.benchmark.helper;
import java.sql.*;

public class SqlConnect {
	
	private static Connection con = null;
	private static String url = "jdbc:mysql://localhost:3306/";
	private static String dbName = "dbpedia";
	private static String driver = "com.mysql.jdbc.Driver";
	private static String userName = "admin"; 
	private static String password = "admin";
	
	public static Connection openConnection(){
					
			try {
				Class.forName(driver).newInstance();
				con = DriverManager.getConnection( url+dbName, userName, password);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Connected to database");
			return con;
	}
		
	public static void closeConnection(){
		
			try {
				con.close();
				System.out.println("Disconnected from database");
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
		
	
}


