package org.benchmark.querying;

import java.util.ArrayList;
import java.util.Random;
import org.apache.log4j.Logger;


public class SPARQLQuery{
	
	private static Logger logger = Logger.getLogger(SPARQLQuery.class);

    public String query;
    public int queryId;

    private ArrayList<TopKQuery> topKQueries;

    
    public SPARQLQuery(int QueryId, String Query ){
        
    	query = Query;
        queryId = QueryId;

        //If the query is empty, then we should return with out further processing 
        if((Query.compareTo("") == 0) )
            return;

        topKQueries = new ArrayList<TopKQuery>();

    }


    
    public ArrayList<TopKQuery> getTopKQueries() {
		return topKQueries;
	}



	public void setTopKQueries(ArrayList<TopKQuery> topKQueries) {
		this.topKQueries = topKQueries;
	}



	public TopKQuery selectRandomTopKQuery(){

        Random generator = new Random();
        int randomPosition = generator.nextInt(topKQueries.size());
        return topKQueries.get(randomPosition);
       
    }



}
