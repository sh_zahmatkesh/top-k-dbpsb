package org.benchmark.querying;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import org.apache.log4j.Logger;
import org.benchmark.helper.BenchmarkConfigReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: Mohamed Morsey
 * Date: Nov 30, 2010
 * Time: 8:21:07 PM
 * This class contains a SPARQL _query that will be executed against all triple stores, along with another simple
 * SPARQL _query that will cause some change to the original _query, in order to avoid caching capability of the
 * triplestore. 
 */
public class TopKQuery {

	private String query ;
	private int dbQueryId ;
	private int objectNumber ;
	private int predicateNumber ;
	private int selectivityType ;
	private int limit ;
	
	
	public TopKQuery(String query, int dbQueryId, int objectNumber,int predicateNumber, int selectivityType , int limit) {
		
		super();
		this.query = query;
		this.dbQueryId = dbQueryId;
		this.objectNumber = objectNumber;
		this.predicateNumber = predicateNumber;
		this.selectivityType = selectivityType;
		this.limit = limit;
	}


	
	
	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public int getDbQueryId() {
		return dbQueryId;
	}


	public void setDbQueryId(int dbQueryId) {
		this.dbQueryId = dbQueryId;
	}


	public int getObjectNumber() {
		return objectNumber;
	}


	public void setObjectNumber(int objectNumber) {
		this.objectNumber = objectNumber;
	}


	public int getPredicateNumber() {
		return predicateNumber;
	}


	public void setPredicateNumber(int predicateNumber) {
		this.predicateNumber = predicateNumber;
	}


	public int getSelectivityType() {
		return selectivityType;
	}


	public void setSelectivityType(int selectivityType) {
		this.selectivityType = selectivityType;
	}
	
	
	
	
	
	
}
