package org.benchmark.querying;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.benchmark.helper.SqlConnect;


public class TopKQueryLoader {

	private static Logger logger = Logger.getLogger(QueryLoader.class);
	private static Connection con = SqlConnect.openConnection();

    public static ArrayList<SPARQLQuery> loadQueries(){
       
    	ArrayList<SPARQLQuery> arrQueries = new ArrayList<SPARQLQuery>();
    	ArrayList<String> mainQueries = new ArrayList<String>();
    	
    	Statement stmt;
		try {
			stmt = con.createStatement();
			String LoadMainQueries = "SELECT id , text FROM dbquery ;";
			ResultSet rs1 = stmt.executeQuery(LoadMainQueries);

			while ( rs1.next() ) {
				
				String mainText = rs1.getString("text");
				int id = rs1.getInt("id");
				arrQueries.add(new SPARQLQuery(id, mainText));
			}
			
			for(SPARQLQuery sparqlrQuery : arrQueries){
				
				String LoadQuery = "SELECT text , objectNumber , predicateNumber , selectivityType , lim FROM topkquery WHERE dbQueryId = "+ sparqlrQuery.queryId + " ;";
				ResultSet rs = stmt.executeQuery(LoadQuery);
			
		    	ArrayList<TopKQuery> arrTopKQueries = new ArrayList<TopKQuery>();
			
				while ( rs.next() ) {
					String text = rs.getString("text");
					int dbQueryId = sparqlrQuery.queryId ;
					int objectNumber = rs.getInt("objectNumber");
					int predicateNumber = rs.getInt("predicateNumber");
					int selectivityType = rs.getInt("selectivityType");
					int limit = rs.getInt("lim");
					arrTopKQueries.add (new TopKQuery(text, dbQueryId, objectNumber, predicateNumber, selectivityType, limit));
				}
			
				sparqlrQuery.setTopKQueries(arrTopKQueries);
				
			}
			

			return 	arrQueries;
		
		} catch (SQLException e) {
			logger.error("Cannot read queries from database, due to " + e.getMessage(), e );
			e.printStackTrace();
			return null;
		}
        
               
    }

    
	private static void While(boolean b) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
