package org.benchmark.querying;

public class ExecutionResult {
	
	private double exeTime ;
	private TopKQuery topKQuery;
	
	public double getExeTime() {
		return exeTime;
	}
	public void setExeTime(double exeTime) {
		this.exeTime = exeTime;
	}
	public TopKQuery getTopKQuery() {
		return topKQuery;
	}
	public void setTopKQuery(TopKQuery topKQuery) {
		this.topKQuery = topKQuery;
	}
	
	
	
	
	

}
