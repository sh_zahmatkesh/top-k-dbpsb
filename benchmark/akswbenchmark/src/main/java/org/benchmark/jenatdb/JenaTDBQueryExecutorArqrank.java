package org.benchmark.jenatdb;


import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.engine.main.OpExecutorFactory;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;
import org.apache.log4j.Logger;
import org.benchmark.helper.BenchmarkConfigReader;
import org.benchmark.querying.ExecutionResult;
import org.benchmark.querying.QueryExecutor;
import org.benchmark.querying.SPARQLQuery;
import org.benchmark.querying.TopKQuery;

import sparqlrank.iterators.RankExecutorFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.concurrent.*;

/**
 * Created by IntelliJ IDEA.
 * User: Mohamed Morsey
 * Date: Nov 26, 2010
 * Time: 6:33:42 PM
 * Executes the queries against a JenaTDB
 */
public class  JenaTDBQueryExecutorArqrank implements QueryExecutor {
    private static Logger logger = Logger.getLogger(JenaTDBQueryExecutorArqrank.class);
    private static final ExecutorService THREAD_POOL = Executors.newCachedThreadPool();
    private static File queryExecutionOutputFile;
    private static FileWriter outWriter;
    private static FileWriter errorWriter;
    private static int showResults = 0;

    private static <T> T timedCall(FutureTask<T> task, long timeout, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        THREAD_POOL.execute(task);
        return task.get(timeout, timeUnit);
    }

    public  JenaTDBQueryExecutorArqrank(){
        try{
            queryExecutionOutputFile = new File("Users/master/Documents/jena/apache-jena-2.10.1/NumberOfResults.txt");
            outWriter = new FileWriter(queryExecutionOutputFile);
        }
        catch(Exception exp){

        }
        
        logger.info("*********************************start initializer*********************************************");
		String directory = "tdb";
		Dataset dataset = TDBFactory.createDataset(directory);

		// assume we want the default model, or we could get a named model here
		Model tdb = dataset.getDefaultModel();
		
		// read the input file - only needs to be done once
		String source = "benchmark_10.nt";
		FileManager.get().readModel( tdb, source, "TURTLE" );
		
		tdb.commit();
		tdb.close();
		logger.info("****************************************end initializer*****************************************");
    }

    public static void executeTestQuery(){
    	String directory = "tdb";
		Dataset dataset = TDBFactory.createDataset(directory);
        QueryExecution execFactory = QueryExecutionFactory .create("SELECT ?s ?p ?o WHERE {?s ?p ?o} LIMIT 100", dataset);

        ResultSet rs =  execFactory.execSelect();
        while(rs.hasNext()){
            QuerySolution sol = rs.nextSolution();
            logger.info("FROM graph" + directory);
            logger.info(sol.get("?s") + "\t" + sol.get("?p") + "\t" + sol.get("?o"));
        }

    }

    public ExecutionResult executeQuery(SPARQLQuery sparqlQuery){
       

    	TopKQuery topk = sparqlQuery.selectRandomTopKQuery() ;
        String query = topk.getQuery();
        ExecutionResult result = new ExecutionResult();
        result.setTopKQuery(topk);
        
        String queryLowerCase = query.toLowerCase();
        long startTime;
        double endTime;
        startTime = System.nanoTime();

        query = removeFromPart(query);
        query = removeCommasBetweenVariables(query);
        query = addPrefixes(query);

        try{

        	String directory = "tdb";
    		Dataset dataset = TDBFactory.createDataset(directory);
            QueryExecution execFactory = QueryExecutionFactory.create(query, dataset);
    		


            logger.info("*****************************JenaTDB ARQ rank EXECUTE_QUERY *****************************");
            
             QueryExecutorWithTimeLimit executorWithLimit = new QueryExecutorWithTimeLimit(execFactory, query);
             FutureTask<Double> queryExecutionTask = new FutureTask<Double>(executorWithLimit);
             try{
            	 result.setExeTime(timedCall(queryExecutionTask, BenchmarkConfigReader.queryExecutionTimeLimit, TimeUnit.MINUTES));
                 return result;
             }
             catch (Exception exp){
                 queryExecutionTask.cancel(true);
                 logger.info("Failed to execute query, and it was forced to terminate after the allowed period has passed" + query);

                 try{
                    errorWriter = new FileWriter(BenchmarkConfigReader.errorFile, true);
                    errorWriter.write(query + "\n");
                    errorWriter.close();
                 }
                 catch(Exception fileExp) {}
                 
                 result.setExeTime(BenchmarkConfigReader.queryExecutionTimeLimit * 60 * 1000000.0);
                 return result;
             }
        }
        catch (Exception exp){
            logger.info("Query " + query + " cannot be executed against Jena TDB");
            result.setExeTime(BenchmarkConfigReader.queryExecutionTimeLimit * 60 * 1000000.0);
            return result;
            
        }
      
    }

    public void executeWarmUpQuery(SPARQLQuery sparqlQuery){
        
    	String query = sparqlQuery.selectRandomTopKQuery().getQuery();
        String queryLowerCase = query.toLowerCase();

        query = removeFromPart(query);
        query = removeCommasBetweenVariables(query);
        query = addPrefixes(query);

        try{

        	String directory = "tdb";
    		Dataset dataset = TDBFactory.createDataset(directory);
    		QueryExecution execFactory = QueryExecutionFactory.create(query, dataset);

            ////////////////////////////////////////////////////////////////////////////
            logger.info("*****************************JenaTDB  ARQ rank EXECUTE WARMUP_QUERY*****************************");

             QueryExecutorWithTimeLimit executorWithLimit = new QueryExecutorWithTimeLimit(execFactory, query);
             FutureTask<Double> queryExecutionTask = new FutureTask<Double>(executorWithLimit);
             try{
                 timedCall(queryExecutionTask, BenchmarkConfigReader.queryExecutionTimeLimit, TimeUnit.MINUTES);
             }
             catch (Exception exp){
                 queryExecutionTask.cancel(true);
                 logger.info("Failed to execute query, and it was forced to terminate after the allowed period has passed" + query);
             }
        }
        catch (Exception exp){
            logger.info("Query " + query + " cannot be executed against Jena TDB");
        }
    }

    
    /**
     * The Jena-TDB triplestore does not accept a FROM-Clause in the SPARQL _query as we must define a Repository
     * so we should remove it, if it exists
     * @param query The _query in which the FROM-Clause may exist
     * @return  The _query after removing the FROM-Clause, so it's ready for execution on Jena-TDB
     */
    private String removeFromPart(String query){
        int fromPosition = query.toLowerCase().indexOf("from");

        //FROM-Clause is not found, so we will return the _query as it is
        if(fromPosition < 0)
            return query;

        //The end of the FROM-Clause is ">", so find it
        int greaterThanPosition = query.indexOf(">");

        //If greater than is not found we will return the _query as it is
        if(greaterThanPosition < 0)
            return query;

        query = query.substring(0, fromPosition) + query.substring(greaterThanPosition+1);

        return query;
    }

    /**
     * The Jena-TDB triplestore does not accept a comma-separated variable list in the SELECT-Clause, i.e. the variables
     * must be separated by space not with comma, so we must replace the commas in SELECT-Clause with spaces
     * @param query The _query in which the variables in SELECT-Clause may be comma-separated
     * @return  The _query after replacing the commas in SELECT-Clause with spaces, so it's ready for execution on Jena-TDB
     */
    private String removeCommasBetweenVariables(String query){
        int wherePos =  query.toLowerCase().indexOf("where");
        StringBuffer queryBuffer = new StringBuffer(query);

        int commaPos = query.lastIndexOf(",", wherePos);
        while(commaPos >= 0){
            queryBuffer.setCharAt(commaPos, ' ');
            commaPos = query.lastIndexOf(",", commaPos-1);
        }
        return queryBuffer.toString();
    }

     /**
     * The Jena-TDB triplestore does not accept some prefixes that are predefined in Virtuoso e.g. dbpprop, so we should
      * add those prefixes to the _query in order to execute it successfully
     * @param query The _query in which we should add the prefixes
     * @return  The _query after adding the required prefixes to it
     * @return  The _query after adding the required prefixes to it
     */
    private String addPrefixes(String query){
         int queryStartPos = -1;
         queryStartPos = query.toLowerCase().indexOf("select");

         if(queryStartPos < 0)
            queryStartPos = query.toLowerCase().indexOf("ask");

         if(queryStartPos < 0)
            queryStartPos = query.toLowerCase().indexOf("construct");

         if(queryStartPos < 0)
            queryStartPos = query.toLowerCase().indexOf("describe");

         if(queryStartPos < 0)
            return query;

         String prefixPart = query.substring(0, queryStartPos);
         if(!prefixPart.contains("dbpprop"))
            query = "PREFIX  dbpprop: <http://dbpedia.org/property/>\n" + query;

         if(!prefixPart.contains("skos"))
            query = "PREFIX  skos: <http://www.w3.org/2004/02/skos/core#>\n"+ query;

         if(!prefixPart.contains("foaf"))
            query = "PREFIX  foaf: <http://xmlns.com/foaf/0.1/>\n" + query;

         if(!prefixPart.contains("rdfs"))
            query = "PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" + query;

         
         return query;
     }

    /**
     * This class implements Callable interface, which provides the ability to place certain time limit on the execution
     * time of a function, so it must end after a specific time limit
     */
    private class QueryExecutorWithTimeLimit implements Callable<Double> {
        QueryExecution queryExecutor;
        String query;

        public QueryExecutorWithTimeLimit(QueryExecution queryExecutor, String query){
            this.query = query;
        }

        public Double call(){
            String queryLowerCase = query.toLowerCase();

            String directory = "tdb";
    		Dataset dataset = TDBFactory.createDataset(directory);
    		
    		Query targetQuery = QueryFactory.create(query, Syntax.syntaxSPARQL_11);
    		OpExecutorFactory customExecutorFactory = new RankExecutorFactory() ;
    		QC.setFactory(ARQ.getContext(), customExecutorFactory) ;
    		Model tdb = dataset.getDefaultModel();

    		this.queryExecutor = QueryExecutionFactory.create(targetQuery, tdb);
            logger.info("Connection opened");
            
            
    		

            long startTime;
            double endTime;
            try{
                 startTime = System.nanoTime();
                 ResultSet rsReturnedResults = null;
                 if(queryLowerCase.contains("select")){

                     rsReturnedResults =  this.queryExecutor.execSelect();
                 }  
                 else if(queryLowerCase.contains("ask"))
                    this.queryExecutor.execAsk();
                 else if(queryLowerCase.contains("describe"))
                    this.queryExecutor.execDescribe();
                 else
                    this.queryExecutor.execConstruct();





                
                endTime = (System.nanoTime() - startTime)/1000.0;
                logger.info("Query = " + query);
                this.queryExecutor.close();

                logger.info("Connection closed");

                 logger.info("Query executed successfully");
                 return endTime;
             }
             catch (Exception exp){
                 logger.error("Query = " + query + " was not executed successfully", exp);
                 this.queryExecutor.abort();
                 this.queryExecutor.close();
                 logger.info("Connection closed");
                 return BenchmarkConfigReader.queryExecutionTimeLimit * 60 * 1000000.0;//if the query is not executed successfully, we should consider it as taking too much time
             }
        }
    }

}
