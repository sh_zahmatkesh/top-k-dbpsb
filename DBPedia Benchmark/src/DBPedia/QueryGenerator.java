package DBPedia;

import java.util.ArrayList;

public class QueryGenerator {
	
	public String generateTopKQuery ( String query, Criterion [] Criteria, ArrayList<String> weights, int limit){ 
		
		// generate SELECT clause
		String scoringFunction = generateScoringFunction( Criteria, weights);
		
		if (scoringFunction.equals("noScoringFunction"))
			return "noTopkQuery";
		
		String selectVariable = generateSelectVariable ( Criteria );
		String selectClause = "\n SELECT "+ selectVariable + scoringFunction ;
		
		//generate WHERE clause
		String triplePatterns =  generateTriplePatterns( Criteria );
		String whereClause = "\n WHERE { " + triplePatterns;
		
		//generate new query
		int selectIndex = query.indexOf("SELECT");
		int WhereIndex = query.indexOf("WHERE");
		String begin = query.substring(0, selectIndex);
		String end = query.substring(query.indexOf("{")) ; 
		end = end.substring(1) ; 
		
		String result = begin.concat(selectClause);
		result = result.concat(whereClause);
		result = result.concat(end);
		
		// remove LIMIT clause if it exist
		if  (result.indexOf("LIMIT") != -1){
			result = result.substring(0, result.indexOf("LIMIT"));
		}
		
		String OrderClause = "\n ORDER BY ?score \n LIMIT " + limit ;
		result = result.concat(OrderClause);
		
		return result;
		
	}
	
	private String generateScoringFunction( Criterion [] Criteria,  ArrayList<String> weights ){
		
		String scoringFunction =" ( ";
		for ( int i = 0 ; i < Criteria.length ; i++){
			 
			 Criterion crit = new Criterion();
			 crit = Criteria[i];
			 
			 if (crit.getMin().equals("") || crit.getMax().equals("") )
				 return "NoScoringFunction";
				 
			 String norm = "( " + weights.get(i) + "* ( ?o" + i +" - " + getXsdValue(crit.getMin()) + " ) / ( " + getXsdValue(crit.getMax()) + " - " + getXsdValue(crit.getMin()) + " ) ) ";
			 scoringFunction = scoringFunction +  norm + " + ";
			
		}
		if (scoringFunction.lastIndexOf("+") != -1)
			scoringFunction = scoringFunction.substring(0 , scoringFunction.lastIndexOf("+")) ;
		
		scoringFunction = scoringFunction + " AS ?score ) ";
		//System.out.println ("Scoring Function : " + scoringFunction);
		return scoringFunction;
		
	}

	private String generateSelectVariable ( Criterion [] Criteria ){
		
		String selectVariable = "";
		for ( int i = 0 ; i < Criteria.length ; i++){
			
			Criterion crit = new Criterion();
			crit = Criteria[i];
			Boolean duplicate = false; 
			
			for ( int j = 0 ; j < i ; j++){
				 
				Criterion prevCrit = new Criterion();
				prevCrit = Criteria[j];
				if (crit.getVariable().equals(prevCrit.getVariable()))
					duplicate = true;
			}
			if (! duplicate )
				selectVariable = selectVariable + " ?" + crit.getVariable();
				
		 }
		// System.out.println ("select variable : " + selectVariable);
		 return selectVariable ;
	}

	private String generateTriplePatterns ( Criterion [] Criteria ){
		
		String triplePatterns = "";
		for ( int i = 0 ; i < Criteria.length ; i++){
			 
			 Criterion crit = new Criterion();
			 crit = Criteria[i];
			 String tp = " ?" + crit.getVariable() + " <" + crit.getPredicate() + "> ?o" + i +" .\n " ;
			 triplePatterns = triplePatterns + tp;
		 }
		// System.out.println ("Triple Patterns : \n " + triplePatterns);
		 return triplePatterns ; 
		
	}

	private String getXsdValue (String value){
		
		String xsdValue = "";
		if (!value.equals("")) {
			String val = value.substring(0, value.indexOf("^"));
			String type = value.substring(value.indexOf("#")+1);
			xsdValue = "xsd:" + type + "(\"" + val + "\") " ;
			
		}
		return xsdValue;
	}
}
