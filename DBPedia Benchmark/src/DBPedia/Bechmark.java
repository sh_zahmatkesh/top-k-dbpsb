package DBPedia;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;


public class Bechmark {
	
	private static QueryProcess qp = new QueryProcess();
	private static QueryGenerator qg = new QueryGenerator();
	static Connection con = qp.getCon();
	
	public static void main(String[] args) {
		
				
		qp.processAllQueries();
		
		for ( int queryId = 1 ; queryId <= 25 ; queryId++){
		
			generateTopkQueries (queryId);
		
		}// for queryId
		
	}

	private static void generateTopkQueries (int queryId){
		
		Random randomGenerator = new Random();
		int sampleQuery = 10;     	// number of generated queries for each db query and each type of selectivity
		int selectivityType = 0;   	// 0=mix, 1=very low, 2=low, 3=medium, 4=high, 5=very high
			
		for ( selectivityType = 0 ; selectivityType <= 5 ; selectivityType ++){
				
			for ( int i= 0 ; i < sampleQuery ; i++ ){
			
				// get all criteria
				ArrayList<Criterion> criteria = getCriteriaOfQuery (queryId, selectivityType) ;
				System.out.println("------------------------------------\nSelectivity : " + selectivityType + "\n criteria size :" + criteria.size());
				if (criteria.size() == 0)
					continue;
					
				// generate random number for the number of criteria 
				int randomPredicateNumber = randomGenerator.nextInt( Math.min( 3 , criteria.size() ) );
				Criterion [] c = new Criterion [randomPredicateNumber+1] ; 
					
				// get 1-3 criteria
				for (int j = 0; j <= randomPredicateNumber ; j++){
					int randomInt = randomGenerator.nextInt(criteria.size());
					//System.out.println ("rand = " + randomInt );
					c[j] = criteria.get(randomInt);
				}
				
				// generate random weights
				ArrayList<String> weights = getRandomWeights( randomPredicateNumber +1 );
					
				// generate Top-k query
				String topKQuery = 	qg.generateTopKQuery(QueryText.QUERY_11, c, weights, 100);
				System.out.println ("topKQuery: \n"+topKQuery);	
					
				// save new top-k query in data base
				if (!topKQuery.equals("noTopkQuery")){
					try {	
						Statement stmt = con.createStatement();
						String sqlQuery =	"INSERT INTO topkquery ( text, dbQueryId, objectNumber, predicateNumber, selectivityType ) " +
												"VALUES ( '" +  topKQuery +"', "+ queryId  + " , "+  getObjectNumber(c) + " , "+ c.length + ", "+ selectivityType +  " ); ";
						int res = stmt.executeUpdate(sqlQuery);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
					
			}// for sampleQuery
				
		}// for selectivityType
		
	}
	
	private static ArrayList<Criterion> getCriteriaOfQuery (int queryId, int selectivityType){
		
		ArrayList<Criterion> criteria = new ArrayList<Criterion>();
		
		String VarIdQuery = "SELECT rankablepredicate.id , rankablepredicate.url , rankablepredicate.max, rankablepredicate.min , variable.name , rankablepredicate.selectivity " +
				"FROM rankablepredicate , variable " +
				"WHERE rankablepredicate.variableId = variable.id and queryId =" + queryId ;
		
		switch (selectivityType) {
		
		case 1: //very low
			VarIdQuery = VarIdQuery.concat(" and rankablepredicate.selectivity  <= 20 ;" );
			break;
		case 2: //low
			VarIdQuery= VarIdQuery.concat( " and rankablepredicate.selectivity > 20 and rankablepredicate.selectivity <= 40 ;" );
			break;
		case 3://medium
			VarIdQuery= VarIdQuery.concat( " and rankablepredicate.selectivity > 40 and rankablepredicate.selectivity <= 60 ;" );
			break;
		case 4://high
			VarIdQuery= VarIdQuery.concat( " and rankablepredicate.selectivity > 60 and rankablepredicate.selectivity <= 80 ;" );
			break;
		case 5://very high
			VarIdQuery= VarIdQuery.concat( " and rankablepredicate.selectivity > 80 and rankablepredicate.selectivity <= 100 ;" );
			break;
		default: 
			VarIdQuery= VarIdQuery.concat( " ;" );
            break;
		}
		
		try { 
			Statement stmt = con.createStatement();
			//System.out.println("selectivity query : \n " +VarIdQuery);
			java.sql.ResultSet rs =stmt.executeQuery(VarIdQuery);
			
			while( rs.next() ){
				int id = rs.getInt ("id");
				String url = rs.getString("url");
				String max = rs.getString("max");
				String min = rs.getString("min");
				String variableName = rs.getString("name");
				double selectivity = rs.getDouble ("selectivity");
				
				Criterion crit = new Criterion();
				crit.setPredicate(url);
				crit.setMax(max);
				crit.setMin(min);
				crit.setVariable(variableName);
				crit.setQueryNumber( String.valueOf(queryId) );
				crit.setSelectivity(String.valueOf(selectivity ));
				criteria.add(crit);	 
			}
			
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return criteria;
	}

	private static ArrayList<String> getRandomWeights( int number ){
		
		Random randomGenerator = new Random();
		ArrayList<String> weights = new ArrayList<String>();
		
		if (number == 1)
			weights.add("1");
		
		if (number == 2){
			int w1 = randomGenerator.nextInt(100);
			int w2 = 100 - w1 ;
			weights.add(String.valueOf((double)w1/100) );
			weights.add( String.valueOf((double)w2/100) );
		}
		
		if (number == 3){
			int w1 = randomGenerator.nextInt(100);
			int w2 = randomGenerator.nextInt(100-w1);
			int w3 = 100 - w1 - w2;
			weights.add( String.valueOf((double)w1/100) );
			weights.add( String.valueOf((double)w2/100) );
			weights.add( String.valueOf((double)w3/100) );	
		}
		
		//System.out.println ("weights:"+ weights.toString());	
		
		return  weights;
		
	}

	private static int getObjectNumber (Criterion [] c){
		
		int objectNumber = c.length;
		for ( int i = 0 ; i < c.length - 1 ; i++){
			
			if ( c[i].getVariable().equals( c[i+1].getVariable() ) ){
				objectNumber--;
			}
				
		}
		//System.out.println("objectNubmer:" + objectNumber);
		return objectNumber;
	}


}
