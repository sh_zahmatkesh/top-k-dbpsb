package DBPedia;

public class Criterion {
	
	private String queryNumber ;
	private String variable;
	private String predicate;
	private String selectivity;
	private String min;
	private String max;
	/**
	 * @return the queryNumber
	 */
	public String getQueryNumber() {
		return queryNumber;
	}
	/**
	 * @param queryNumber the queryNumber to set
	 */
	public void setQueryNumber(String queryNumber) {
		this.queryNumber = queryNumber;
	}
	/**
	 * @return the predicate
	 */
	public String getPredicate() {
		return predicate;
	}
	/**
	 * @param predicate the predicate to set
	 */
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	
	public String getSelectivity() {
		return selectivity;
	}
	public void setSelectivity(String selectivity) {
		this.selectivity = selectivity;
	}
	/**
	 * @return the min
	 */
	public String getMin() {
		return min;
	}
	/**
	 * @param min the min to set
	 */
	public void setMin(String min) {
		this.min = min;
	}
	/**
	 * @return the max
	 */
	public String getMax() {
		return max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(String max) {
		this.max = max;
	}
	
	/**
	 * @return the variable
	 */
	public String getVariable() {
		return variable;
	}
	/**
	 * @param variable the variable to set
	 */
	public void setVariable(String variable) {
		this.variable = variable;
	}
	
	
}
