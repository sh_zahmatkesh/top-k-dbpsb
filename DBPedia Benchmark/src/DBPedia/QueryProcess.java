package DBPedia;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import DataBase.SqlConnect;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;


public class QueryProcess {
	
	
	private static Connection con = SqlConnect.openConnection();

	
	public static void processAllQueries() {
		
		//processQuery (1 , QueryText.QUERY_1 , 1);			// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso 22023 Error SR580: RDF box refers to row with RO_ID = 52884101 of table DB.DBA.RDF_OBJ, but no such row in the table
		
		//processQuery (2 , QueryText.QUERY_2 , 1);			// done completely  ( no rankable variable!)
		
		//processQuery (3 , QueryText.QUERY_3 , 1);			// done completely  ( no rankable variable!)
		
		//processQuery (4 , QueryText.QUERY_4 , 1);			// Caused by: HttpException: 500 SPARQL Request Failed
															// Virtuoso RDFXX Error .....: IRI ID 191202487 does not match any known IRI in __rdf_sqlval_of_obj()
		
		//processQuery (5 , QueryText.QUERY_5 , 1);			//Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															//Virtuoso 42000 Error The estimated execution time 4498 (sec) exceeds the limit of 3000 (sec).
		
		//processQuery (6 , QueryText.QUERY_6 , 1); 		// done completely 
		
		//processQuery (7 , QueryText.QUERY_7 , 1);			// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso 42000 Error The estimated execution time 35151 (sec) exceeds the limit of 3000 (sec).
		
		//processQuery (8 , QueryText.QUERY_8 , 1);			// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso 22023 Error SR580: RDF box refers to row with RO_ID = 50856794 of table DB.DBA.RDF_OBJ, but no such row in the table
		
		//processQuery (9 , QueryText.QUERY_9 , 1); 		// done completely 
		
		//processQuery (10 , QueryText.QUERY_10 , 1);		// done completely 
		
		processQuery (11 , QueryText.QUERY_11 , 1);		// done completely 
		
		//processQuery (12 , QueryText.QUERY_12 , 1);			// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso RDFXX Error .....: IRI ID 191202487 does not match any known IRI in __rdf_sqlval_of_obj()

		//processQuery (13 , QueryText.QUERY_13 , 1);		//Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															//Virtuoso RDFXX Error .....: IRI ID 191202487 does not match any known IRI in __rdf_sqlval_of_obj()
		
		//processQuery (14, QueryText.QUERY_14 , 1);		// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso RDFXX Error .....: IRI ID 191202487 does not match any known IRI in __rdf_sqlval_of_obj()
		
		//processQuery (15, QueryText.QUERY_15 , 1);
		
		//processQuery (16, QueryText.QUERY_16 , 1);
		
		//processQuery (17, QueryText.QUERY_17 , 1);   		// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso 22023 Error SR580: RDF box refers to row with RO_ID = 52884271 of table DB.DBA.RDF_OBJ, but no such row in the table 
		
		//processQuery (18, QueryText.QUERY_18 , 1);

		//processQuery (19, QueryText.QUERY_19 , 1);
		
		//processQuery (20, QueryText.QUERY_20 , 1);
		
		//processQuery (21, QueryText.QUERY_21 , 1);
		
		//processQuery (22, QueryText.QUERY_22 , 1);
		
		//processQuery (23, QueryText.QUERY_23 , 1);
		
		//processQuery (24, QueryText.QUERY_24 , 1);		// Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															// Virtuoso 42000 Error The estimated execution time 205194944 (sec) exceeds the limit of 3000 (sec).
		
		//processQuery (25 , QueryText.QUERY_25 , 1);		//Exception in thread "main" HttpException: HttpException: 500 SPARQL Request Failed
															//Virtuoso 42000 Error The estimated execution time 3017 (sec) exceeds the limit of 3000 (sec).
		
	}
	

	private static void processQuery (int id , String dbpQuery , int dbpId){
		
		addQueryToDataBase(id, dbpQuery, dbpId);
		System.out.println("finish adding query to db");
		ArrayList<String> vars = findQueryVariables (id , dbpQuery);
		System.out.println("finish finding variable");
		processRankableVariables(id, dbpQuery, vars);
		System.out.println("finish statistic");
	}

	
	private static void addQueryToDataBase(int id , String dbpQuery , int dbpId){
		 
		// 1- run the dbpQuery to get the number of result of the query
		String countQuery = getCountQuery(dbpQuery);
		String dbpQueryForStore = dbpQuery.replace("'", "''");
		//System.out.println (dbpQueryForStore);
		Query query = QueryFactory.create( countQuery );
		QueryExecution qexec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query);
		ResultSet results = qexec.execSelect();
		int resultNumber = -1;
		for (; results.hasNext();) {
			 
	    	 QuerySolution soln = results.nextSolution();
	    	 Iterator varNames = soln.varNames();
	    	 String varName;
	    	 for (; varNames.hasNext();) {
	    		 varName = (String) varNames.next();
	    		 if (varName.equals("count")){
	    			 resultNumber = Integer.parseInt(getValue( soln.get(varName).toString()) );
	    		 }
	    		 
	    	 }
	     }
		 qexec.close() ;
		 
		
		 // 2- store information related to query in DB
		Statement stmt;
		try {
			stmt = con.createStatement();
			String checkExistanceQuery = "SELECT * FROM dbquery WHERE id =" + id + ";";
			java.sql.ResultSet rs = stmt.executeQuery(checkExistanceQuery);
			
			if (rs.getRow()==0){
				String sqlQuery ="INSERT INTO dbquery (id , text, dbpId , resultNumber) VALUES (" + id +", '"+ dbpQueryForStore + "' , "+ dbpId + "," + resultNumber + " ); ";
				int res = stmt.executeUpdate(sqlQuery);
				//con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	private static ArrayList<String> findQueryVariables (int id , String dbpQuery){
		
		ArrayList<String> vars = new ArrayList<String>();
		
		//1- split the query to tokens
		String [] tokens = dbpQuery.split("[ \t\n]");
		for ( int i = 0; i < tokens.length ; i++ ){
			
			// check if the token is variable, and if yes put it in the varsOfType list
			if ( !tokens[i].equals("") && tokens[i].charAt(0) =='?'  ){
				
				tokens[i] = tokens[i].substring(1);  // remove ?
				if (tokens[i].indexOf('.')!=-1)		// remove other characters like . } 
					tokens[i] = tokens[i].substring(0 , (tokens[i].length()-1) );
				if (!vars.contains(tokens[i])) 
					vars.add(tokens[i]);
		
			}
		}
		
		// 2- put all variables in DB
		try {
			Statement stmt = con.createStatement();
			for ( int i = 0 ; i < vars.size() ; i++ ){

				String sqlQuery ="INSERT INTO variable( name, queryId ) VALUES ( '" + vars.get(i)+"', "+ id + " ); ";
				int res = stmt.executeUpdate(sqlQuery);
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return vars;
		
	}

	
	private static void processRankableVariables( int id , String dbpQuery , ArrayList<String> vars ){
		
		for ( int i = 0 ; i < vars.size() ; i++ ){
			
			//find all the rankable predicate related to var(i)
			String [] floatPredicatesArray = findRankablePredicate(dbpQuery, vars.get(i) , QueryText.FLOAT_DATATYPE);
			String [] timePredicatesArray = findRankablePredicate(dbpQuery, vars.get(i) , QueryText.TIME_DATE_DATATYPE);
			
			//compute some statistic for each rankable predicate			
			for (int j=0 ; j<(floatPredicatesArray.length / 2) ; j++){			
				
				// find the max and min value mapped to each rankable predicate
				findBoundsOfRabkablePredicate(id , dbpQuery , vars.get(i) , floatPredicatesArray[2*j] , floatPredicatesArray[1 + 2*j], QueryText.FLOAT_DATATYPE);
				
				// find the distribution of values mapped to each rankable predicate
				findDistributionOfRabkablePredicate(id , dbpQuery , vars.get(i) , floatPredicatesArray[2*j], QueryText.FLOAT_DATATYPE);
			}	
			
			//compute some statistic for each rankable predicate			
			for (int j=0 ; j<(timePredicatesArray.length / 2) ; j++){			
				
				// find the max and min value mapped to each rankable predicate
				findBoundsOfRabkablePredicate(id , dbpQuery , vars.get(i) , timePredicatesArray[2 * j] , timePredicatesArray[1 + 2*j] , QueryText.TIME_DATE_DATATYPE);
				
				// find the distribution of values mapped to each rankable predicate
				findDistributionOfRabkablePredicate(id , dbpQuery , vars.get(i) , timePredicatesArray[2 * j] , QueryText.TIME_DATE_DATATYPE);
			}			
			
		}
		
	}
	
	
	private static String [] findRankablePredicate(String dbpQuery , String var , String dataType ){
		
		//create new query 
		String varQuery = getNumericalDatatypeQuery(var, dbpQuery , dataType);
		
		//get all the rankable predicate related to each variables				
		Query query = QueryFactory.create(varQuery); //Syntax.syntaxSPARQL_11);
			 QueryExecution qexec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query);
			 ResultSet results = qexec.execSelect();
			 String predicatesString = "";
			 
			 for (; results.hasNext();) {
				 
		    	 QuerySolution soln = results.nextSolution();
		    	 Iterator varNames = soln.varNames();
		    	 String varName;
		    	 for (; varNames.hasNext();) {
		    		 varName = (String) varNames.next();
		    		 predicatesString += soln.get(varName) + ";";
		    	 }
		     }
		     
			 qexec.close() ;
			 
		String [] predicatesArray = predicatesString.split(";");
		return predicatesArray;
	}
	
			
	private static void findBoundsOfRabkablePredicate(int id , String dbpQuery , String var , String predicateName , String predicateValue  , String dataType){
		
		//1- create new query with min , max and compute the max and min mapped to each rankable predicate
		String predicateQuery = getPredicateMinMaxQuery(predicateName, var, dbpQuery, dataType);				
		Query q = QueryFactory.create(predicateQuery); //Syntax.syntaxSPARQL_11);
		QueryExecution qexec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", q);
		ResultSet results = qexec.execSelect();
		String min = "", max = "";
		for (; results.hasNext();) {			
	    	QuerySolution soln = results.nextSolution();
	    	Iterator varNames = soln.varNames();
	    	String varName;
	    	for (; varNames.hasNext();) {
	    		varName = (String) varNames.next();
	    		if (varName.equals("max"))
	    			max = soln.get(varName).toString();
	    		else if (varName.equals("min"))
	    			min = soln.get(varName).toString(); 
	    	 }
	    }
		qexec.close() ;
		
		// 2- put statistic on aech predicate in DB
		try {
			// get variable Id
			Statement stmt = con.createStatement();
			String VarIdQuery = "SELECT id FROM variable WHERE name = '" + var + "' and  queryId=" + id + " ;";
			java.sql.ResultSet rs =stmt.executeQuery(VarIdQuery);
			rs.first();
			int varId = rs.getInt(1);
			
			// get main query result number
			String propertyIdQuery = "SELECT resultNumber FROM dbquery WHERE id = " + id + " ;";
			java.sql.ResultSet rs1 =stmt.executeQuery(propertyIdQuery);
			rs1.first();
			int resultNumber = rs1.getInt(1);
			
			// add rankable variable
			String sqlQuery =	"INSERT INTO rankablepredicate ( url , max , min , resultNumber, variableId ,dataType , selectivity ) " +
								"VALUES ( '" + predicateName+"', '"+ max  + "', '"+  min + "' , "+ getValue(predicateValue)+ ", "+ varId + ",'"+ dataType +"' , " + ( 100 * Double.parseDouble(getValue(predicateValue)) / resultNumber) + " ); ";
			int res = stmt.executeUpdate(sqlQuery);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	
	private static void findDistributionOfRabkablePredicate(int id , String dbpQuery , String var , String predicateName, String dataType){	
		
		// 1- get the distribution of each predicate
		String predicateDisQuery = getPredicateDisQuery(predicateName, var, dbpQuery ,dataType);			//create new query 
		Query q = QueryFactory.create(predicateDisQuery); //Syntax.syntaxSPARQL_11);
		QueryExecution qexec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", q);
		ResultSet results = qexec.execSelect();
		String predicateDisString = "";
		 
		for (; results.hasNext();) {
			 
	    	 QuerySolution soln = results.nextSolution();
	    	 Iterator varNames = soln.varNames();
	    	 String varName;
	    	 for (; varNames.hasNext();) {
	    		 varName = (String) varNames.next();
	    		 predicateDisString += soln.get(varName) + ";";
	    	 }
	    } 
		qexec.close() ; 
		
		// 2- put the distribution of each predicate in DB
		String [] predicateDisArray = predicateDisString.split(";");
		for (int j=0 ; j<(predicateDisArray.length / 2) ; j++){			
			 
			try {
					Statement stmt = con.createStatement();
					// get vars Id
					String varIdQuery = "SELECT id FROM variable WHERE name = '" + var + "' and  queryId=" + id + " ;";
					java.sql.ResultSet rs =stmt.executeQuery(varIdQuery);
					rs.first();
					int varId = rs.getInt(1);
					
					// get property Id
					String propertyIdQuery = "SELECT id FROM rankablepredicate WHERE url = '" + predicateName + "' and  variableId=" + varId + " ;";
					java.sql.ResultSet rs1 =stmt.executeQuery(propertyIdQuery);
					rs1.first();
					int propertyId = rs1.getInt(1);
					
					//store property distribution in DB
					String sqlQuery =	"INSERT INTO rankablepredicatedistribution ( object , count , predicateId ) " +
										"VALUES ( '" + predicateDisArray[1+ 2 * j] + "' , " + getValue(predicateDisArray[ 2 * j] )+ " , " + propertyId + " ) ; ";
					int res = stmt.executeUpdate(sqlQuery);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}			
		
		
	}

	
	
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//                                                               query creation part   
//----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	// create a new query to find the number of results of the main query  
	private static String getCountQuery (String query){
		
		int selectIndex = query.indexOf("SELECT");
		int WhereIndex = query.indexOf("WHERE");
		String begin = query.substring(0, selectIndex);
		String end = query.substring(WhereIndex);
		String result = begin.concat(QueryText.SELECT_FOR_COUNT);
		result = result.concat(end);
		return result;	
	}

	
	// create a new query to find all the rankable variable
	private static String getNumericalDatatypeQuery (String var ,String query , String dataType){
		
		int selectIndex = query.indexOf("SELECT");
		int WhereIndex = query.indexOf("WHERE");
		String begin = query.substring(0, selectIndex);
		String end = query.substring(WhereIndex);
		String result = begin.concat(QueryText.SELECT_FOR_NUMERICAL_DATATYPE);
		result = result.concat(end);
		result = result.substring(0, result.lastIndexOf("}"));
		
		if (dataType.equals(QueryText.FLOAT_DATATYPE))
			result = result +" ?" + var + QueryText.FILTER_FOR_NUMERICAL_FLOAT_DATATYPE;
		else if (dataType.equals(QueryText.TIME_DATE_DATATYPE))
			result = result +" ?" + var + QueryText.FILTER_FOR_NUMERICAL_TIME_DATATYPE;

		return result;
		
	}
	
	
	// create a new query to find the max and min for a specified predicate
	private static String getPredicateMinMaxQuery(String predicate, String var, String query, String dataType) {
		
		int selectIndex = query.indexOf("SELECT");
		int WhereIndex = query.indexOf("WHERE");
		String begin = query.substring(0, selectIndex);
		String end = query.substring(WhereIndex);
		String result = begin.concat(QueryText.SELECT_FOR_MIN_MAX_PREDICATE);
		result = result.concat(end);
		result = result.substring(0, result.lastIndexOf("}"));
		
		if (dataType.equals(QueryText.FLOAT_DATATYPE))
			result = result +" ?" + var + " <" + predicate + "> "+ QueryText.FILTER_FOR_MIN_MAX_FLOAT_PREDICATE;
		else if (dataType.equals(QueryText.TIME_DATE_DATATYPE))
			result = result +" ?" + var + " <" + predicate + "> "+ QueryText.FILTER_FOR_MIN_MAX_TIME_PREDICATE;
		
		
		return result;
	}
	
	
	// create a new query to find the distribution for a specified predicate
	private static String getPredicateDisQuery(String predicate, String var, String query, String dataType) {
		
		int selectIndex = query.indexOf("SELECT");
		int WhereIndex = query.indexOf("WHERE");
		String begin = query.substring(0, selectIndex);
		String end = query.substring(WhereIndex);
		String result = begin.concat(QueryText.SELECT_FOR_DISTRIBUTION_PREDICATE);
		result = result.concat(end);
		result = result.substring(0, result.lastIndexOf("}"));
		
		if (dataType.equals(QueryText.FLOAT_DATATYPE))
			result = result +" ?" + var + " <" + predicate + "> "+ QueryText.FILTER_FOR_DISTRIBUTION_FLOAT_PREDICATE;
		else if (dataType.equals(QueryText.TIME_DATE_DATATYPE))
			result = result +" ?" + var + " <" + predicate + "> "+ QueryText.FILTER_FOR_DISTRIBUTION_TIME_PREDICATE;
		
		return result;
	}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private static String getValue (String url){
		
		if (!url.equals(""))
			return url.substring(0, url.indexOf("^"));
		else 
			return "";
	}


	
	public static Connection getCon() {
		return con;
	}


	
	public static void setCon(Connection con) {
		QueryProcess.con = con;
	}
	
	

}
